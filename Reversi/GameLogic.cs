﻿ using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    public enum Direction { Up, Down, Left, Right, UpRight, UpLeft, DownRight, DownLeft, NoDirection }

    public class GameLogic
    {
        Cell[,] cells;

        byte horizontalCells, verticalCells;

        public GameLogic(Cell[,] cells)
        {
            this.cells = cells;
            this.horizontalCells = (byte)cells.GetLength(0);
            this.verticalCells = (byte)cells.GetLength(1);
        }

        public GameLogic Copy()
        {
            return new GameLogic((Cell[,])cells.Clone());
        }

        public Cell[,] Grid
        {
            get
             {
                return cells;
            }
        }
        public int GridWidth
        {
            get
            {
                return cells.GetLength(0);
            }
        }
        public int GridHeight
        {
            get
            {
                return cells.GetLength(1);
            }
        }

        public bool NewStep(Tuple<byte,byte,float> t, Player turn)
        {
            return NewStep(t.Item1, t.Item2, turn);
        }

        public bool NewStep(byte x, byte y, Player turn)
        {
            if (cells[x, y] == Cell.Blue || cells[x, y] == Cell.Red || cells[x, y] == Cell.None) return false;

            if (turn == Player.Red)
            {
                if (cells[x, y] == Cell.PossibleBlue) return false;
            }
            if (turn == Player.Blue)
            {
                if (cells[x, y] == Cell.PossibleRed) return false;
            }

            cells[x, y] = (Cell)turn;

            updateEnclosedCells(x, y, turn);

            UpdateGrid();
            return true;
        }

        public int SpacesLeft()
        {
            int returnValue = 0;
            for (byte x = 0; x < horizontalCells; x++)
            {
                for (byte y = 0; y < verticalCells; y++)
                {
                    if (cells[x, y] != Cell.Red && cells[x, y] != Cell.Blue) returnValue++;
                }
            }
            return returnValue;
        }



        public bool PossibilityInTurn(Player turn)
        {
            Cell c;
            if (turn == Player.Blue) c = Cell.PossibleBlue; else if (turn == Player.Red) c = Cell.PossibleRed; else throw new ArgumentException();
            for (byte x = 0; x < horizontalCells; x++)
            {
                for (byte y = 0; y < verticalCells; y++)
                {
                    if (cells[x, y] == c || cells[x, y] == Cell.PossibleBoth) return true;
                }
            }
            return false;
        }

        public Player WhoHasMostPieces()
        {
            short differenceFromRed = 0;
            for (byte x = 0; x < horizontalCells; x++)
            {
                for (byte y = 0; y < verticalCells; y++)
                {
                    if (cells[x, y] == Cell.Blue) differenceFromRed++; else if (cells[x, y] == Cell.Red) differenceFromRed--;
                }
            }
            if (differenceFromRed > 0) return Player.Blue;
            if (differenceFromRed < 0) return Player.Red;
            return Player.None;
        }
        public Tuple<int,int> PieceCount()
        {
            short red = 0;
            short blue = 0;
            for (byte x = 0; x < horizontalCells; x++)
            {
                for (byte y = 0; y < verticalCells; y++)
                {
                    if (cells[x, y] == Cell.Blue) blue++; else if (cells[x, y] == Cell.Red) red++;
                }
            }

            return new Tuple<int,int>(red, blue);
        }


        public void UpdateGrid()
        {
            for (byte x = 0; x < horizontalCells; x++)
            {
                for (byte y = 0; y < verticalCells; y++)
                {
                    UpdateCellPossibilities(x, y);
                }
            }
        }

        public void CleanGrid()
        {
            for (byte x = 0; x < horizontalCells; x++)
            {
                for (byte y = 0; y < verticalCells; y++)
                {
                    if (cells[x, y] == Cell.PossibleBlue || cells[x, y] == Cell.PossibleRed || cells[x, y] == Cell.PossibleBoth)
                    {
                        cells[x, y] = Cell.None;
                    }
                }
            }
        }

        public void UpdateCellPossibilities(byte x, byte y)
        {
            if (cells[x, y] != Cell.Red && cells[x, y] != Cell.Blue)
            {
                cells[x, y] = Cell.None;
                
                if (checkIfCanCloseIn(x, y, Player.Blue))
                {
                     cells[x, y] = Cell.PossibleBlue;    
                }
                if (checkIfCanCloseIn(x, y, Player.Red))
                {
                    if (cells[x, y] == Cell.PossibleBlue) cells[x, y] = Cell.PossibleBoth;
                    else cells[x, y] = Cell.PossibleRed;
                }
            }
        }

        private List<Direction> checkIfClosingIn(byte x, byte y, Player p)
        {
            List<Direction> output = new List<Direction>();
            if (checkIfClosedIn(x, y, p, Direction.Up)) output.Add(Direction.Up);
            if (checkIfClosedIn(x, y, p, Direction.Down)) output.Add(Direction.Down);
            if (checkIfClosedIn(x, y, p, Direction.Right)) output.Add(Direction.Right);
            if (checkIfClosedIn(x, y, p, Direction.Left)) output.Add(Direction.Left);
            if (checkIfClosedIn(x, y, p, Direction.UpRight)) output.Add(Direction.UpRight);
            if (checkIfClosedIn(x, y, p, Direction.UpLeft)) output.Add(Direction.UpLeft);
            if (checkIfClosedIn(x, y, p, Direction.DownRight)) output.Add(Direction.DownRight);
            if (checkIfClosedIn(x, y, p, Direction.DownLeft)) output.Add(Direction.DownLeft);
            return output;
        }

        private bool checkIfClosedIn(byte x, byte y, Player p, Direction d, bool otherColor = false)
        {
            if (d != Direction.NoDirection)
            {
                Tuple<sbyte, sbyte> directionTuple = getCoordsFromDirection(d);
                byte newX = (byte)(x + directionTuple.Item1);
                byte newY = (byte)(y + directionTuple.Item2);
                Cell newCell;

                if (newX < horizontalCells && newY < verticalCells && newX >= 0 && newY >= 0)
                {
                    newCell = cells[newX, newY];
                }
                else return false;

                //Debug.WriteLine($"Player: {p}, Direction: {d}, X: {x}, Y: {y}, NewCell: {newCell}, Amount: {otherColor}");

                switch (p)
                {
                    case (Player.Blue):
                        if (newCell == Cell.Red) return checkIfClosedIn(newX, newY, p, d, true);
                        else
                        {
                            if (newCell == Cell.Blue) return otherColor;
                            else return false;
                        }

                    case (Player.Red):
                        if (newCell == Cell.Blue) return checkIfClosedIn(newX, newY, p, d, true);
                        else
                        {
                            if (newCell == Cell.Red) return otherColor;
                            else return false;
                        }

                    default:
                        return false;
                }
            }
            else return false;
        }


        private bool checkIfCanCloseIn(byte x, byte y, Player p)
        {
            if (checkIfClosingIn(x, y, p).Count != 0)
            {
                return true;
            }
            return false;
        }

        private void updateEnclosedCells(byte x, byte y, Player p)
        {
            foreach (Direction d in checkIfClosingIn(x, y, p))
            {
                updateEnclosedCells(x, y, d);
            }
        }

        private void updateEnclosedCells(byte x, byte y, Direction d)
        {
            Cell c = cells[x, y];
            Cell otherC = Cell.None;
            if (c == Cell.Blue)
            {
                otherC = Cell.Red;
            }
            else if (c == Cell.Red)
            {
                otherC = Cell.Blue;
            }
            Tuple<sbyte, sbyte> directionTuple = getCoordsFromDirection(d);
            byte newX = (byte)(x + directionTuple.Item1);
            byte newY = (byte)(y + directionTuple.Item2);

            if (cells[newX, newY] == c)
            {
                return;
            }
            else if (cells[newX, newY] == otherC && otherC != Cell.None)
            {
                cells[newX, newY] = c;
                updateEnclosedCells(newX, newY, d);
            }
        }

        private Tuple<sbyte, sbyte> getCoordsFromDirection(Direction d)
        {
            switch (d)
            {
                case (Direction.Up):
                    return new Tuple<sbyte, sbyte>(0, -1);
                case (Direction.Down):
                    return new Tuple<sbyte, sbyte>(0, 1);
                case (Direction.Left):
                    return new Tuple<sbyte, sbyte>(-1, 0);
                case (Direction.Right):
                    return new Tuple<sbyte, sbyte>(1, 0);
                case (Direction.UpRight):
                    return new Tuple<sbyte, sbyte>(1, -1);
                case (Direction.UpLeft):
                    return new Tuple<sbyte, sbyte>(-1, -1);
                case (Direction.DownRight):
                    return new Tuple<sbyte, sbyte>(1, 1);
                case (Direction.DownLeft):
                    return new Tuple<sbyte, sbyte>(-1, 1);
                case (Direction.NoDirection):
                    return new Tuple<sbyte, sbyte>(0, 0);
                default:
                    throw new ArgumentException();
            }
        }
    }
}
