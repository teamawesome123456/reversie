﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reversi
{
    class AI
    {
        public static Tuple<byte, byte, float> GetBestMove(GameLogic game, Difficulty depth, Player turn)
        {
            return GetBestMove(game, (int)depth, turn);
        }

        public static Tuple<byte, byte, float> GetBestMove(GameLogic game, int Depth, Player turn)
        {
            float score = -100000; //The initial value is very low to ensure that any possible moves (even if the score is low) still outrank not moving at all.
            byte bestX = 0;
            byte BestY = 0;
            Cell possibility = Cell.None;

            if (turn == Player.Blue) possibility = Cell.PossibleBlue;
            if (turn == Player.Red) possibility = Cell.PossibleRed;

            for (byte x = 0; x < game.GridWidth; x++)
            {
                for (byte y = 0; y < game.GridHeight; y++)
                {
                    Cell c = game.Grid[x, y];

                    if (Depth == 1)
                    {
                        if (c == possibility || c == Cell.PossibleBoth)
                        {
                            
                            GameLogic newGame = game.Copy();
                            bool hasStepped = newGame.NewStep(x, y, turn);
                            if (!hasStepped)
                            {
                                Debug.WriteLine("An impossible turn has occured, On a Possible Turn!");
                            }
                            
                            float currentScore = GetGridScore(newGame, turn);
                            Debug.WriteLine($"AI: PLayer: {turn}, X: {x}, Y: {y}, And possible, Score: {score}, This step: {currentScore}");

                            if (currentScore >= score)
                            {
                                bestX = x;
                                BestY = y;
                                score = currentScore;
                            }
                        }
                    }
                    else
                    {
                        if (c == possibility || c == Cell.PossibleBoth)
                        {
                            GameLogic newGame = game.Copy();
                            newGame.NewStep(x, y, turn);
                            Tuple<byte, byte, float> currentScore = GetBestMove(newGame, Depth - 1, turn); //Recursive

                            if (currentScore.Item3 > score)
                            {
                                bestX = x;
                                BestY = y;
                                score = currentScore.Item3;
                            }
                        }
                    }
                }
            }
            Debug.WriteLine("Tried move at " + bestX + ", " + BestY + ".");
            return new Tuple<byte, byte, float>(bestX, BestY, score);
        }

        public static float GetGridScore(GameLogic game, Player turn)
        {
            float score = 0;

            for (byte x = 0; x < game.GridWidth; x++)
            {
                for (byte y = 0; y < game.GridHeight; y++)
                {
                    score += GetCellScore(game, x, y, turn);
                }
            }

            return score;
        }

        public static float GetCellScore(GameLogic game, byte x, byte y, Player turn)
        {
            Cell invertTurn = Cell.None;
            Cell c = game.Grid[x, y];
            float cellMultiplier = 1;
            if (x == 0 || x == game.GridWidth - 1) cellMultiplier *= 1.5f;
            if (y == 0 || y == game.GridHeight - 1) cellMultiplier *= 1.5f;
            if (turn == Player.Blue) invertTurn = Cell.Red;
            else if (turn == Player.Red) invertTurn = Cell.Blue;

            if (c == (Cell)turn)
            {
                return cellMultiplier;
            }
            else if (c == invertTurn)
            {
                return cellMultiplier * -1;
            }

            return 0;
        }
    }
}

