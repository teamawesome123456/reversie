﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reversi
{
    public partial class GraphForm : Form
    {
        public List<int> One;
        public List<int> Two;

        public GraphForm()
        {
            InitializeComponent();

            this.One = new List<int>();
            this.Two = new List<int>();
            graphPanel.Paint += draw;
            this.Resize += formResize;
        }

        public void InvalidateGraph()
        {
            graphPanel.Invalidate();
        }

        private void draw(object sender, PaintEventArgs pea)
        {
            Graphics gr = pea.Graphics;
            gr.FillRectangle(new SolidBrush(Color.White), 0, 0, graphPanel.Width, graphPanel.Height);

            float barW = (float)graphPanel.ClientSize.Width / One.Count / 2f;
            float barH = (float)graphPanel.ClientSize.Height / (Math.Max(One.Max(), Two.Max()));
            float h = graphPanel.ClientSize.Height;
            Brush oneBrush = new SolidBrush(Color.Red);
            Brush twoBrush = new SolidBrush(Color.Blue);
            Debug.WriteLine($"BarH: {barH}, BarW: {barW}, h: {h}");
            int j;

            for (int i = 0; i < One.Count; i++)
            {
                j = i * 2;
                gr.FillRectangle(oneBrush, j * barW, h - (One[i] * barH), barW, One[i] * barH);
                gr.FillRectangle(twoBrush, ((float)(j+1)) * barW, h - (Two[i] * barH), barW, Two[i] * barH);
            }
        }

        private void formResize(object sender, EventArgs ea)
        {
            graphPanel.Size = this.ClientSize;
            graphPanel.Invalidate();
        }
    }
}
