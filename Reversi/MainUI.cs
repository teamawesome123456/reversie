﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

//Dit is het hoofdbestand
//
//De UI is iets veranderd met de opdracht:
//  *   Textboxes voor de grootte van het speelveld
//  *   UI voor AI instellingen (welke player de AI is en hoe "goed" hij is)
//  *   De knop help geeft een "tutorial" (eigenlijk gewoon de opdrachtbeschrijving)
//  *   Er is een checkbox om de mogelijke stappen te zien (leek ons een logischere vorm dan een knop)
//  *   Er is een knop om een graph te laten zien waarin per stap (van het actieve potje) het aantal stenen per kleur staat
//
//De git server voor eventuele problemen:
//https://Nonnominandus@bitbucket.org/teamawesome123456/reversie.git
//Het opstart object is in principe de .sln file en anders Reversi.csproj
//
//Alle forms zijn volligde resiable (en maken daar dan redelijk optimaal gebruik van)
//Dit is de reden dat er uberhaupt geen variabelen zijn voor de breedte/hoogte van een steen


namespace Reversi
{
    public enum Cell { None, Red, Blue, PossibleRed, PossibleBlue, PossibleBoth };
    public enum Player { None, Red, Blue }; //The same names at the same position so cells can be cast to players and the other way around
    public enum Difficulty { Normal = 1, Hard, Master }

    public partial class MainUI : Form
    {
        Cell[,] cells;
        Player turn = Player.Blue;
        GameLogic game;
        int horizontalCells;
        int verticalCells;
        const bool viewNone = false; //To see crosses on places where cell = None
        const bool viewPossibleColours = false; //To see the Possibles in different colours
        bool passed;
        Player AIPlayer;
        Difficulty difficulty;
        List<int> numberOfPiecesRed = new List<int>();
        List<int> numberOfPiecesBlue = new List<int>();
        GraphForm gf = new GraphForm();

        private int userInputHorizontalCells
        {
            get
            {
                try
                {
                    int output = Convert.ToInt16(widthTextBox.Text);
                    if (output < 3) output = 3;
                    return output;
                }
                catch
                {
                    MessageBox.Show("The value for the width is not correct.");
                    return -1;
                }
            }
            set
            {
                widthTextBox.Text = value.ToString();
            }
        }
        private int userInputVerticalCells
        {
            get
            {
                try
                {
                    int output = Convert.ToInt16(heightTextBox.Text);
                    if (output < 3) output = 3;
                    return output;
                }
                catch
                {
                    MessageBox.Show("The value for the height is not correct.");
                    return -1;
                }
            }
            set
            {
                heightTextBox.Text = value.ToString();
            }
        }

        public MainUI()
        {
            InitializeComponent();

            //Initialize Event handlers
            gamePanel.Paint += updateGamePanel;
            gamePanel.MouseClick += panelClick;
            this.Resize += formResize;
            viewPossible.CheckedChanged += checkedChanged;

            //Initialize variables
            this.DoubleBuffered = true;
            AIListBox.SelectedIndex = 0;
            DifficultyListBox.SelectedIndex = 0;

            startNewGame();
        }

        private void startNewGame(object sender = null, EventArgs e = null)
        {
            //Initialze variables
            turn = Player.Blue;
            horizontalCells = userInputHorizontalCells;
            verticalCells = userInputVerticalCells;
            cells = new Cell[horizontalCells, verticalCells];
            game = new GameLogic(cells);
            AIPlayer = getAISelected();
            difficulty = getDifficultySelected();
            resetGraphForm();

            //Get starting data
            startValuesCells();

            //Clear UI
            game.UpdateGrid();
            invalidateGamePanel();
            formResize();
            updateNumberLists();

            Debug.WriteLine($"A new game, AI: {AIPlayer}, Dif: {difficulty}");

            //if AI is first, let it make a move
            if (AIPlayer == Player.Blue)
            {
                game.NewStep(AI.GetBestMove(game, difficulty, AIPlayer), AIPlayer);
                turn = Player.Red;
                updateNumberLists();
            }
            updateStatusLabels();
        }

        private void startValuesCells()
        {
            //Places four pieces at the middle of the screen
            int x_mid = horizontalCells / 2;
            int x_1 = x_mid - 1 + horizontalCells % 2;
            int x_2 = x_mid + horizontalCells % 2;

            int y_mid = verticalCells / 2;
            int y_1 = y_mid - 1 + verticalCells % 2;
            int y_2 = y_mid + verticalCells % 2;

            cells[x_1, y_1] = Cell.Blue;
            cells[x_1, y_2] = Cell.Red;
            cells[x_2, y_2] = Cell.Blue;
            cells[x_2, y_1] = Cell.Red;
        }

        private bool doStep(Tuple<Byte, Byte, float> t)
        {
            return doStep(t.Item1, t.Item2);
        }

        private bool doStep(byte x, byte y)
        {
            bool canStep = game.NewStep(x, y, turn);
            
            if (canStep)
            {
                flipTurn();

                game.UpdateGrid();
                invalidateGamePanel();
                updateStatusLabels();
                this.Refresh();

                if (turn == AIPlayer)
                {
                    Thread.Sleep(AIDelayTrackBar.Value);
                    doAIStep();
                }

                invalidateGamePanel();
                updateStatusLabels();
            }

            //Check status for future turns
            if (!game.PossibilityInTurn(turn))
            {
                if (passed || game.SpacesLeft() == 0)
                {
                    //Game ended
                    MessageBox.Show($"{game.WhoHasMostPieces()} has won!");
                }
                else
                {
                    //Has to pass this turn
                    MessageBox.Show($"{turn} has no possible moves this turn. \nClick ok to continue.");
                    flipTurn();
                    if (turn == AIPlayer) doAIStep();
                    updateStatusLabels();
                    passed = true;
                }
            }
            else
            {
                passed = false;

            }

            if (canStep) updateNumberLists();

            return canStep;
        }

        private void doAIStep()
        {
            flipTurn();
            bool stepDone = game.NewStep(AI.GetBestMove(game, difficulty, AIPlayer), AIPlayer);

            Debug.WriteLine("Move gotten for " + AIPlayer + ". Move was succesful: " + stepDone);
            
            game.UpdateGrid();
        }

        private void checkedChanged(object o, EventArgs ea)
        {
            game.UpdateGrid();
            invalidateGamePanel();
        }

        private void invalidateGamePanel(object o = null, EventArgs ea = null)
        {
            gamePanel.Invalidate();
        }

        private void updateGamePanel(object o, PaintEventArgs pea)
        {
            //Haal info uit de array (cells) en vul het datagridview
            int w = gamePanel.Size.Width;
            int h = gamePanel.Size.Height;

            Graphics output = pea.Graphics;

            int cell_width = w / horizontalCells;
            int cell_height = h / verticalCells;

            updateGraphForm();

            //Draw Lines
            Pen linePen = new Pen(Color.Black, 1);

            for (int i = 0; i <= horizontalCells; i++)
            {
                //Draw Lines
                output.DrawLine(linePen, cell_width * i, 0, cell_width * i, h);
            }
            for (int i = 0; i <= verticalCells; i++)
            {
                //Draw Lines
                output.DrawLine(linePen, 0, cell_height * i, w, cell_height * i);
            }

            //Draw pieces
            Brush redBrush = new SolidBrush(Color.Red);
            Brush blueBrush = new SolidBrush(Color.Blue);
            Pen possiblilityPen = new Pen(Color.Gray, 1);
            Pen redPen = new Pen(Color.Red, 1);
            Pen bluePen = new Pen(Color.Blue, 1);

            for (int x = 0; x < horizontalCells; x++)
            {
                for (int y = 0; y < verticalCells; y++)
                {
                    switch (cells[x, y])
                    {
                        case Cell.Red:
                            output.FillEllipse(redBrush, cell_width * x, cell_height * y, cell_width, cell_height);
                            break;
                        case Cell.PossibleRed:
                            if (turn == Player.Red && viewPossible.Checked)
                            {
                                output.DrawEllipse(viewPossibleColours ? redPen : possiblilityPen, cell_width * (float)(x + 0.25), cell_height * (float)(y + 0.25), (float)(cell_width * 0.5), (float)(cell_height * 0.5));
                            }
                            break;
                        case Cell.Blue:
                            output.FillEllipse(blueBrush, cell_width * x, cell_height * y, cell_width, cell_height);
                            break;
                        case Cell.PossibleBlue:
                            if (turn == Player.Blue && viewPossible.Checked)
                            {
                                output.DrawEllipse(viewPossibleColours ? bluePen : possiblilityPen, cell_width * (float)(x + 0.25), cell_height * (float)(y + 0.25), (float)(cell_width * 0.5), (float)(cell_height * 0.5));
                            }
                            break;
                        case Cell.PossibleBoth:
                            if (viewPossible.Checked)
                            {
                                output.DrawEllipse(possiblilityPen, cell_width * (float)(x + 0.25), cell_height * (float)(y + 0.25), (float)(cell_width * 0.5), (float)(cell_height * 0.5));
                            }
                            break;
                        case Cell.None:
                            if (viewPossible.Checked && viewNone)
                            {
                                output.DrawLine(possiblilityPen, cell_width * (float)(x + 0.4), cell_height * (float)(y + 0.4), cell_width * (float)(x + 0.6), cell_height * (float)(y + 0.6));
                                output.DrawLine(possiblilityPen, cell_width * (float)(x + 0.6), cell_height * (float)(y + 0.4), cell_width * (float)(x + 0.4), cell_height * (float)(y + 0.6));
                            }
                            break;
                    }
                }
            }
        }

        private void updateStatusLabels()
        {
            if (turn == AIPlayer)
            {
                statusLabel.Text = "Turn: AI";
                statusLabel.ForeColor = Color.Purple;
            }
            else if (turn == Player.Red)
            {
                statusLabel.Text = "Turn: Red";
                statusLabel.ForeColor = Color.Red;
            }
            else if (turn == Player.Blue)
            {
                statusLabel.Text = "Turn: Blue";
                statusLabel.ForeColor = Color.Blue;
            }
            else
            {
                statusLabel.Text = "Turn: ?????";
                statusLabel.ForeColor = Color.Black;
            }

            piecesRed.Text = $"Red: {countInCells(Cell.Red)}";
            piecesBlue.Text = $"Blue: {countInCells(Cell.Blue)}";
        }

        private int countInCells(Cell c)
        {
            int output = 0;
            foreach (Cell cell in cells)
            {
                if (cell == c) output++;
            }
            return output;
        }

        //Events voor User Input
        private void panelClick(object o, MouseEventArgs mea)
        {
            byte x_cell = (byte)Math.Floor(((double)mea.X / gamePanel.Size.Width) * horizontalCells);
            byte y_cell = (byte)Math.Floor(((double)mea.Y / gamePanel.Size.Height) * verticalCells);

            Debug.WriteLine($"X: {mea.X}, Y: {mea.Y}, XC: {x_cell}, YC: {y_cell}");

            doStep(x_cell, y_cell);
        }

        private void formResize(object o = null, EventArgs ea = null)
        {
            UIPanel.Location = new Point(0, 0);
            gamePanel.Location = new Point(0, UIPanel.Height);

            int new_w = this.ClientSize.Width - AIPanel.Width;
            int new_h = this.ClientSize.Height - UIPanel.Height;

            double ratio = horizontalCells / (double)verticalCells;

            if (new_w < new_h * ratio)
            {
                gamePanel.Height = (int)(new_w / ratio);
                gamePanel.Width = (int)(new_w);
            }
            else
            {
                gamePanel.Height = (int)(new_h);
                gamePanel.Width = (int)(new_h * ratio);
            }

            AIPanel.Location = new Point(new_w, 0);

            invalidateGamePanel();
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();
            helpForm.Show();
        }

        private Player getAISelected()
        {
            Debug.WriteLine($"AI: {AIListBox.SelectedItem}");
            switch (AIListBox.SelectedItem)
            {
                case ("No AI"):
                    return Player.None;
                case ("Blue AI"):
                    return Player.Blue;
                case ("Red AI"):
                    return Player.Red;
                default:
                    return Player.None;
            }
        }

        private Difficulty getDifficultySelected()
        {
            Debug.WriteLine($"AI: {DifficultyListBox.SelectedItem}");
            switch (DifficultyListBox.SelectedItem)
            {
                case ("Normal"):
                    return Difficulty.Normal;
                case ("Hard"):
                    return Difficulty.Hard;
                case ("Master"):
                    return Difficulty.Master;
                default:
                    return Difficulty.Normal;
            }
        }

        private void flipTurn()
        {
            if (turn == Player.Red) turn = Player.Blue; else turn = Player.Red;
        }

        private void AIDelayTrackBar_Scroll(object sender, EventArgs e)
        {
            AIDelayLabel.Text = $"Delay: {AIDelayTrackBar.Value}";
        }

        private int count(Cell[,] ar, Cell elem)
        {
            int output = 0;
            for (int i = 0; i < ar.GetLength(0); i++)
            {
                for (int j = 0; j < ar.GetLength(0); j++)
                {
                    if (ar[i, j] == elem) output += 1;
                }
            }
            return output;
        }

        private int count(object[] ar, object elem)
        {
            int output = 0;
            foreach(object e in ar)
            {
                if (e == elem) output += 1;
            }
            return output;
        }

        private void GraphButton_Click(object sender, EventArgs e)
        {
            gf.Show();
        }

        private void updateNumberLists()
        {
            numberOfPiecesRed.Add(count(cells, Cell.Red));
            numberOfPiecesBlue.Add(count(cells, Cell.Blue));
        }

        private void updateGraphForm()
        {
            gf.One = numberOfPiecesRed;
            gf.Two = numberOfPiecesBlue;
            gf.InvalidateGraph();
        }

        private void resetGraphForm()
        {
            numberOfPiecesRed = new List<int>();
            numberOfPiecesBlue = new List<int>();
            updateGraphForm();
        }
    }
}
