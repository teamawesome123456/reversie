﻿namespace Reversi
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusLabel = new System.Windows.Forms.Label();
            this.piecesRed = new System.Windows.Forms.Label();
            this.piecesBlue = new System.Windows.Forms.Label();
            this.startGameButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.heightTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gamePanel = new System.Windows.Forms.Panel();
            this.UIPanel = new System.Windows.Forms.Panel();
            this.viewPossible = new System.Windows.Forms.CheckBox();
            this.AIListBox = new System.Windows.Forms.ListBox();
            this.AILabel = new System.Windows.Forms.Label();
            this.DifficultyLabel = new System.Windows.Forms.Label();
            this.DifficultyListBox = new System.Windows.Forms.ListBox();
            this.AIDelayTrackBar = new System.Windows.Forms.TrackBar();
            this.AIPanel = new System.Windows.Forms.Panel();
            this.GraphButton = new System.Windows.Forms.Button();
            this.AIDelayLabel = new System.Windows.Forms.Label();
            this.UIPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AIDelayTrackBar)).BeginInit();
            this.AIPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(11, 5);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(48, 13);
            this.statusLabel.TabIndex = 0;
            this.statusLabel.Text = "no game";
            // 
            // piecesRed
            // 
            this.piecesRed.AutoSize = true;
            this.piecesRed.ForeColor = System.Drawing.Color.Red;
            this.piecesRed.Location = new System.Drawing.Point(30, 32);
            this.piecesRed.Name = "piecesRed";
            this.piecesRed.Size = new System.Drawing.Size(39, 13);
            this.piecesRed.TabIndex = 2;
            this.piecesRed.Text = "Red: 0";
            // 
            // piecesBlue
            // 
            this.piecesBlue.AutoSize = true;
            this.piecesBlue.ForeColor = System.Drawing.Color.Blue;
            this.piecesBlue.Location = new System.Drawing.Point(30, 56);
            this.piecesBlue.Name = "piecesBlue";
            this.piecesBlue.Size = new System.Drawing.Size(40, 13);
            this.piecesBlue.TabIndex = 3;
            this.piecesBlue.Text = "Blue: 0";
            // 
            // startGameButton
            // 
            this.startGameButton.Location = new System.Drawing.Point(253, 26);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(75, 23);
            this.startGameButton.TabIndex = 4;
            this.startGameButton.Text = "Start Game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Click += new System.EventHandler(this.startNewGame);
            // 
            // helpButton
            // 
            this.helpButton.Location = new System.Drawing.Point(253, 51);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(75, 23);
            this.helpButton.TabIndex = 5;
            this.helpButton.Text = "Help";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // widthTextBox
            // 
            this.widthTextBox.Location = new System.Drawing.Point(179, 29);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(53, 20);
            this.widthTextBox.TabIndex = 6;
            this.widthTextBox.Text = "6";
            // 
            // heightTextBox
            // 
            this.heightTextBox.Location = new System.Drawing.Point(179, 53);
            this.heightTextBox.Name = "heightTextBox";
            this.heightTextBox.Size = new System.Drawing.Size(53, 20);
            this.heightTextBox.TabIndex = 7;
            this.heightTextBox.Text = "6";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Width:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(132, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Height:";
            // 
            // gamePanel
            // 
            this.gamePanel.Location = new System.Drawing.Point(0, 80);
            this.gamePanel.Name = "gamePanel";
            this.gamePanel.Size = new System.Drawing.Size(350, 350);
            this.gamePanel.TabIndex = 10;
            // 
            // UIPanel
            // 
            this.UIPanel.Controls.Add(this.viewPossible);
            this.UIPanel.Controls.Add(this.startGameButton);
            this.UIPanel.Controls.Add(this.statusLabel);
            this.UIPanel.Controls.Add(this.label2);
            this.UIPanel.Controls.Add(this.piecesRed);
            this.UIPanel.Controls.Add(this.label1);
            this.UIPanel.Controls.Add(this.piecesBlue);
            this.UIPanel.Controls.Add(this.heightTextBox);
            this.UIPanel.Controls.Add(this.helpButton);
            this.UIPanel.Controls.Add(this.widthTextBox);
            this.UIPanel.Location = new System.Drawing.Point(0, 0);
            this.UIPanel.Name = "UIPanel";
            this.UIPanel.Size = new System.Drawing.Size(350, 80);
            this.UIPanel.TabIndex = 11;
            // 
            // viewPossible
            // 
            this.viewPossible.AutoSize = true;
            this.viewPossible.Location = new System.Drawing.Point(202, 6);
            this.viewPossible.Name = "viewPossible";
            this.viewPossible.Size = new System.Drawing.Size(126, 17);
            this.viewPossible.TabIndex = 10;
            this.viewPossible.Text = "View Possible Moves";
            this.viewPossible.UseVisualStyleBackColor = true;
            // 
            // AIListBox
            // 
            this.AIListBox.FormattingEnabled = true;
            this.AIListBox.Items.AddRange(new object[] {
            "No AI",
            "Blue AI",
            "Red AI"});
            this.AIListBox.Location = new System.Drawing.Point(14, 20);
            this.AIListBox.Name = "AIListBox";
            this.AIListBox.Size = new System.Drawing.Size(59, 43);
            this.AIListBox.TabIndex = 11;
            // 
            // AILabel
            // 
            this.AILabel.AutoSize = true;
            this.AILabel.Location = new System.Drawing.Point(14, 4);
            this.AILabel.Name = "AILabel";
            this.AILabel.Size = new System.Drawing.Size(17, 13);
            this.AILabel.TabIndex = 12;
            this.AILabel.Text = "AI";
            // 
            // DifficultyLabel
            // 
            this.DifficultyLabel.AutoSize = true;
            this.DifficultyLabel.Location = new System.Drawing.Point(14, 78);
            this.DifficultyLabel.Name = "DifficultyLabel";
            this.DifficultyLabel.Size = new System.Drawing.Size(60, 13);
            this.DifficultyLabel.TabIndex = 13;
            this.DifficultyLabel.Text = "AI Difficulty";
            // 
            // DifficultyListBox
            // 
            this.DifficultyListBox.FormattingEnabled = true;
            this.DifficultyListBox.Items.AddRange(new object[] {
            "Normal",
            "Hard",
            "Master"});
            this.DifficultyListBox.Location = new System.Drawing.Point(14, 94);
            this.DifficultyListBox.Name = "DifficultyListBox";
            this.DifficultyListBox.Size = new System.Drawing.Size(59, 43);
            this.DifficultyListBox.TabIndex = 14;
            // 
            // AIDelayTrackBar
            // 
            this.AIDelayTrackBar.Location = new System.Drawing.Point(14, 143);
            this.AIDelayTrackBar.Maximum = 1000;
            this.AIDelayTrackBar.Name = "AIDelayTrackBar";
            this.AIDelayTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.AIDelayTrackBar.Size = new System.Drawing.Size(45, 179);
            this.AIDelayTrackBar.TabIndex = 15;
            this.AIDelayTrackBar.TickFrequency = 10;
            this.AIDelayTrackBar.Value = 400;
            this.AIDelayTrackBar.Scroll += new System.EventHandler(this.AIDelayTrackBar_Scroll);
            // 
            // AIPanel
            // 
            this.AIPanel.Controls.Add(this.GraphButton);
            this.AIPanel.Controls.Add(this.AIDelayLabel);
            this.AIPanel.Controls.Add(this.DifficultyListBox);
            this.AIPanel.Controls.Add(this.AIDelayTrackBar);
            this.AIPanel.Controls.Add(this.AIListBox);
            this.AIPanel.Controls.Add(this.AILabel);
            this.AIPanel.Controls.Add(this.DifficultyLabel);
            this.AIPanel.Location = new System.Drawing.Point(350, 0);
            this.AIPanel.Name = "AIPanel";
            this.AIPanel.Size = new System.Drawing.Size(90, 376);
            this.AIPanel.TabIndex = 16;
            // 
            // GraphButton
            // 
            this.GraphButton.Location = new System.Drawing.Point(6, 341);
            this.GraphButton.Name = "GraphButton";
            this.GraphButton.Size = new System.Drawing.Size(75, 23);
            this.GraphButton.TabIndex = 11;
            this.GraphButton.Text = "Graph";
            this.GraphButton.UseVisualStyleBackColor = true;
            this.GraphButton.Click += new System.EventHandler(this.GraphButton_Click);
            // 
            // AIDelayLabel
            // 
            this.AIDelayLabel.AutoSize = true;
            this.AIDelayLabel.Location = new System.Drawing.Point(14, 325);
            this.AIDelayLabel.Name = "AIDelayLabel";
            this.AIDelayLabel.Size = new System.Drawing.Size(58, 13);
            this.AIDelayLabel.TabIndex = 17;
            this.AIDelayLabel.Text = "Delay: 400";
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 431);
            this.Controls.Add(this.AIPanel);
            this.Controls.Add(this.UIPanel);
            this.Controls.Add(this.gamePanel);
            this.Name = "MainUI";
            this.Text = "Reversi";
            this.UIPanel.ResumeLayout(false);
            this.UIPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AIDelayTrackBar)).EndInit();
            this.AIPanel.ResumeLayout(false);
            this.AIPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Label piecesRed;
        private System.Windows.Forms.Label piecesBlue;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.TextBox heightTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel gamePanel;
        private System.Windows.Forms.Panel UIPanel;
        private System.Windows.Forms.CheckBox viewPossible;
        private System.Windows.Forms.ListBox AIListBox;
        private System.Windows.Forms.Label AILabel;
        private System.Windows.Forms.Label DifficultyLabel;
        private System.Windows.Forms.ListBox DifficultyListBox;
        private System.Windows.Forms.TrackBar AIDelayTrackBar;
        private System.Windows.Forms.Panel AIPanel;
        private System.Windows.Forms.Label AIDelayLabel;
        private System.Windows.Forms.Button GraphButton;
    }
}

